from django.contrib.auth.models import AbstractUser
from django.db import models


class Gender(models.Model):
    value = models.CharField(max_length=255)

    def __str__(self):
        return self.value


class User(AbstractUser):
    gender = models.ForeignKey(Gender, on_delete=models.PROTECT)
